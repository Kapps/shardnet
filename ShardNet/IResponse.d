﻿module ShardNet.IResponse;
public import ShardNet.IRequest;
public import ShardTools.StringMap;
 
/// Determines the encoding of a file.
public enum EncodingType {
	/// No encoding, plain-text.
	Identity = 0,
	/// Encoded using the deflate algorithm.
	Deflate = 1,
	/// Encoded using gzip.
	Gzip = 2
}

/// Represents a response to a request.
interface IResponse  {
	// TODO: Add ResponseChunks. Then Response just has the request and status code, and the chunks have the rest.
	// Keep in mind the issues with sending the status code, then having an exception or something in the later chunks...
	// So, if that happens, just add a redirect header to a 500 page maybe.

	/// Gets the request that caused this response.
	@property IRequest Request();

	/// Gets the status code that this response returned.
	@property uint StatusCode() const;

	/// Gets the headers for the response message.
	@property StringMap Headers();
	
	/// Gets the data for this repsonse.
	/// It is possible for the response to resize, edit, or delete, the content data, and thus this should never be stored.
	/// If this response uses a chunked transfer type, this can either be null, or the first chunk (implementation defined).
	/// It is invalid to access this if the response is complete.
	@property const(ubyte[]) Content() const;
	
	/// Gets the current encoding of this response. The server may decide to encode it differently.
	@property EncodingType ContentEncoding() const;

	/// Notifies this instance that the response has been fully sent, and is capable of releasing any resources used.	
	void NotifyComplete();

	/+ Everything here is todo. Note that they likely won't be in IResponse.
	+  The idea is basically to support chunked encoding like http does.
	+	void SendChunk(MessageChunk Chunk);
	+	void CompleteResponse(StringMap AdditionalHeaders);
	+/

	const final bool opEquals(const Object Other) {
		void* first = cast(void*)&this;
		void* second = cast(void*)&Other;
		return first == second;
	}		
}