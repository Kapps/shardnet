﻿module ShardNet.Backend;
import ShardNet.IRequestHandler;
public import ShardNet.IResponse;
public import ShardNet.IRequest;


/// Provides a backend for a server capable of handling requests.
abstract class Backend  {

// TODO: Implement a CancelRequest (remember RequestID is used for this)!
// Generally used for when someone disconnects before receiving a response, or a response times out.

public:
	/// Initializes a new instance of the Backend object.
	/// Params:
	/// 	RequestHandler = The handler to use for handling requests.
	this(IRequestHandler RequestHandler) {
		this.RequestHandler = RequestHandler;
	}

	/// Notifies this backend that a request was received.
	/// Params:
	/// 	Request = The request being received.
	/// 	Tag = Any additional info the backend may need to process the request. Passed in to SendResponse after the request is handled.
	protected final void NotifyRequestReceived(IRequest Request, Object Tag) {
		RequestHandler.QueueRequest(Request, (IResponse Response) {
			SendResponse(Response, Tag);
		});
	}		

	/// Notifies this backend that the given request was terminated.
	/// Params:
	/// 	Request = The request being terminated.
	protected void NotifyTerminateRequested(IRequest Request) {
		Request.Terminate();
	}

	/// Sends a response to the frontend after a request is processed.
	/// Params:
	/// 	Response = The response being sent.
	/// 	Tag = The application-specific info passed into NotifyRequestReceived.
	protected abstract void SendResponse(IResponse Response, Object Tag);

	/// Gets or sets the request handler for this backend.
	@property protected IRequestHandler RequestHandler() {
		return _RequestHandler;
	}

	/// Ditto
	@property protected void RequestHandler(IRequestHandler Handler) {
		this._RequestHandler = Handler;
	}
	
private:
	IRequestHandler _RequestHandler;	
}