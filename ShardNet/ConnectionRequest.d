﻿module ShardNet.ConnectionRequest;
private import std.socket;


/// Represents a single incoming connection, in theory prior to the completion of a handshake.
/// Because no handshake is done at this point, all values may be spoofed.
struct ConnectionRequest  {

public:
	/// Initializes a new instance of the ConnectionRequest object.
	this(const(ubyte)[] InitialData, const(Address) RemoteEndpoint) {
		this._InitialData = InitialData;
		this._RemoteEndpoint = RemoteEndpoint;
	}

	/// Gets the initial data the remote endpoint sent.
	@property const(ubyte)[] InitialData() {
		return _InitialData;
	}

	/// Gets the address the request sender sent. This address was not confirmed (and thus susceptible to spoofing).
	/// IMPORTANT:
	///		This address may be spoofed! No handshake is completed at this moment; it should not be relied upon.
	@property const(Address) RemoteEndpoint() {
		return _RemoteEndpoint;		
	}
	
private:
	const(ubyte)[] _InitialData;
	const(Address) _RemoteEndpoint;
}