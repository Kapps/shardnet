﻿module ShardNet.IRequest;
public import ShardTools.StringMap;
import std.socket;

/// Indicates the type of a request, either a static file or dynamic page.
enum RequestType {
	/// No request type specified.
	None = 0,
	/// A static file, such as an image or stylesheet.
	Static = 1,
	/// A dynamically generated page.
	Dynamic = 2
}


/// The base interface for a page request.
interface IRequest  {	

	/// Gets any headers that the request sent, or parameters if there are no headers.
	@property StringMap Headers();			

	/// Gets a unique identifier representing this request. Generally, this is just an auto-increment field.
	@property ulong RequestID();	

	/// Indicates whether this request was terminated.
	/// An example of this is the remote host disconnected prior to a response being fully generated.
	@property bool IsTerminated();
	
	/// Indicates the type of this request.
	@property RequestType Type();

	/// Terminates this request, indicating that a response is no longer needed.
	void Terminate();

	const final bool opEquals(const Object Other) {
		void* first = cast(void*)&this;
		void* second = cast(void*)&Other;
		return first == second;
	}		
}