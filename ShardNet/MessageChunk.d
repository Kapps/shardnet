﻿module ShardNet.DataChunk;
private import ShardTools.Buffer;


/// Provides a chunk of data used for a request or response.
struct MessageChunk  {

public:

	/// The request or response ID that this chunk is being sent for.
	const ulong ID;

	/// The number of this chunk. That is, this is the Numberth chunk that has been sent for the request or response with an ID of ID.
	/// The first chunk has an ID of zero.
	const uint Number;

	/// Gets the data contained within this chunk.
	const ubyte[] Data;

	/// Gets whether this chunk is the final chunk in a message.
	const bool IsFinalChunk;	

	/// Creates a new MessageChunk from predefined data.	
	this(in ubyte[] Data, ulong ID, uint Number, bool IsFinalChunk) {
		this.Data = Data;
		this.IsFinalChunk = IsFinalChunk;
		this.Number = Number;
		this.ID = ID;
	}
}