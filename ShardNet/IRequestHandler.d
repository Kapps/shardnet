﻿module ShardNet.IRequestHandler;

public import ShardNet.IRequest;
public import ShardNet.IResponse;

/// The base class for an object that handles request.
interface IRequestHandler {
	/// Processes the given HttpRequest.
	/// Params:
	/// 	Request = The request to process.
	IResponse ProcessRequest(IRequest Request);

	/// Queues the given request to be processed when a worker thread is available.
	/// Params:
	/// 	Request = The request to queue.
	/// 	ResponseHandler = A delegate used to handle the response to the request.
	void QueueRequest(IRequest Request, void delegate(IResponse) ResponseHandler);
}