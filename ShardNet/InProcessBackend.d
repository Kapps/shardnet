﻿module ShardNet.InProcessBackend;
public import ShardNet.IRequestHandler;
import ShardNet.BackendConnection;
public import ShardNet.Backend;


/// Provides a backend running in the same process as the server.
class InProcessBackend : Backend {

public:
	/// Initializes a new instance of the InProcessBackend object.
	this(IRequestHandler RequestHandler) {
		super(RequestHandler);		
	}

	/// Sends a response to the frontend after a request is processed.
	/// Params:
	/// 	Response = The response being sent.
	protected override void SendResponse(IResponse Response, Object Tag) {
		 InProcessBackendConnection Connection = cast(InProcessBackendConnection)Tag;
		 Connection.NotifyResponse(Response);
	}

	/// Sends a request to this backend.
	/// Params:
	/// 	Request = The request to send.
	package void SendRequest(IRequest Request, InProcessBackendConnection Connection) {
		this.NotifyRequestReceived(Request, Connection);
	}

	/// Sends to this backend a request to terminate the request.
	/// Params:
	/// 	Request = The request to terminate.	
	package void SendTerminated(IRequest Request, InProcessBackendConnection Connection) {		
		this.NotifyTerminateRequested(Request);
	}
}

/// Provides a connection to an in-process backend.
class InProcessBackendConnection : BackendConnection {	
	
	/// Creates a connection to the given backend.
	/// Params:
	/// 	Backend = The backend to create a connection to.
	this(InProcessBackend Backend) {
		super(RequestType.Dynamic | RequestType.Static);
		this.Backend = Backend;
	}

	/// Sends a request to this backend. When deriving, the base implementation must be called.
	/// Params:
	/// 	Request = The request being sent.
	override void SendRequest(IRequest Request) {
		super.SendRequest(Request);
		this.Backend.SendRequest(Request, this);
	}

	/// Called when a response is received from the backend.
	/// When deriving, the base implementation must be called.
	/// Params:
	/// 	Response = The response being received.
	protected override void OnResponseReceived(IResponse Response) {
		super.OnResponseReceived(Response);		
	}	

	/// Notifies this connection that a response was received.
	/// Params:
	/// 	Response = The response being received.
	package void NotifyResponse(IResponse Response) {
		this.NotifyResponseReceived(Response);
	}

	/// Gets a value indicating whether the backend is responding and capable of receiving requests.
	/// For an in process backend, this is always true.
	@property override bool IsResponding() const {
		return true;
	}

	/// Sends a request to terminate the given request.
	/// A response should not be expected afterwards.
	/// Params:
	/// 	RequestToTerminate = The request to terminate.
	override void SendTerminateRequest(IRequest RequestToTerminate) {
		super.SendTerminateRequest(RequestToTerminate);

	}

	private InProcessBackend Backend;	
}

