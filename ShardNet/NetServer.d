﻿module ShardNet.NetServer;
private import core.sync.mutex;
version(Windows) private import std.c.windows.winsock;
private import core.atomic;
private import std.stdio;
private import std.parallelism;
private import ShardTools.LinkedList;
private import std.datetime;
private import ShardIO.CallbackOutput;
private import ShardTools.NativeReference;
private import core.thread;
private import ShardTools.ExceptionTools;
public import ShardTools.PrioritizedEvent;
public import ShardTools.Event;
private import std.exception;
public import ShardNet.NetConnection;

enum ServerState {
	Inactive = 0,
	Listening = 1,
	ShutDown = 2
}

mixin(MakeException("InvalidStateException"));

/// An asynchronous server capable of handling zero or more clients, each of which is stored as a NetConnection.
/// The class is templated so that the application may store it's own data about each user by overriding the NetConnection class.
/// So long as the server is listening, it will not be garbage collected.
class NetServer(ConnectionType : NetConnection) {
	
public:

	alias Event!(bool, ConnectionType) ConnectionRequestedEvent;
	alias Event!(void, ConnectionType) ConnectionEvent;
	alias Event!(void, ConnectionType, string) ConnectionClosedEvent;
	alias Event!(void, ushort) ServerListenEvent;
	alias Event!(void, string) ServerShutdownEvent;
	alias Event!(void, ConnectionType, ubyte[]) ConnectionDataEvent;

	/// Initializes a new instance of the NetServer object.
	this(NetProtocol Protocol, IPVersion Version = IPVersion.Both) {		
		this._Protocol = Protocol;
		this._Version = Version;			
		this._Connections = new LinkedList!ConnectionType();
		this._ConnectionRequested = new ConnectionRequestedEvent();
		this._ClientConnected = new ConnectionEvent();
		this._ClientDisconnected = new ConnectionClosedEvent();
		this._ConnectionRejected = new ConnectionEvent();
		this._BegunListening = new ServerListenEvent();
		this._ShuttingDown = new ServerShutdownEvent();
		this._DataReceived = new ConnectionDataEvent();
		this._TimeoutTime = dur!"seconds"(15);
		this.IpToConnectionsLock = new Mutex();
	}

	/// Gets or sets the amount of time that needs to pass for a connection to time out if no message is received.
	@property Duration TimeoutTime() const {
		return _TimeoutTime;
	}

	/// Ditto
	@property void TimeoutTime(Duration Value) {
		_TimeoutTime = Value;
	}

	/// Gets an Event called when any Connection sends data to the server.
	@property final ConnectionDataEvent DataReceived() {
		return _DataReceived;
	}

	/// Gets an Event called when the Server begins listening on any port.	
	@property final ServerListenEvent BegunListening() {
		return _BegunListening;
	}

	/// Gets an Event called when the Server is starting to be shut down through a call to Disconnect.
	@property final ServerShutdownEvent ShuttingDown() {
		return _ShuttingDown;
	}

	/// Gets an Event called when a Connection is requested to the server.
	/// If any of the subscribers return false, the connection is rejected.
	@property final ConnectionRequestedEvent ConnectionRequested() {
		return _ConnectionRequested;
	}

	/// Gets an Event called when a client attempts to connect but is rejected.
	@property final ConnectionEvent ConnectionRejected() {
		return _ConnectionRejected;
	}

	/// Gets an Event called when a Connection is accepted successfully.
	@property final ConnectionEvent ClientConnected() {
		return _ClientConnected;
	}

	/// Gets an Event called when a client disconnects from the server.
	@property final ConnectionClosedEvent ClientDisconnected() {
		return _ClientDisconnected;
	}	

	/// Gets or sets the maximum number of connections a single IP can make to any port on that IP protocol.
	/// For example, if this value is 8, a single IP could make 8 connections maximum to the IPv4 socket (but also another 8 to the IPv6 socket).
	/// The default value is 16. When this value is lowered, clients will not be immediately disconnected.
	/// Instead, they will be refused additional connections until they drop below the threshold.	
	/// If this value is zero, then no new connections will be allowed because they will all have too many to be accepted.
	/// The value size_t.max is a special case that removes the overhead of a lock and check upon connection request, accept, and disconnect.
	@property size_t MaxConnectionsPerIP() const {
		return _MaxConnectionsPerIP;
	}

	/// Ditto
	@property void MaxConnectionsPerIP(size_t Value) {
		_MaxConnectionsPerIP = Value;
	}

	/// Gets the protocol that this server uses.
	@property NetProtocol Protocol() const {
		return _Protocol;
	}

	/// Gets the IP version this Protocol uses.
	@property IPVersion Version() const {
		return _Version;
	}

	/// Gets all of the clients connected to the server.
	@property LinkedList!ConnectionType Connections() {
		return _Connections;
	}

	/// Begins listening for connections on the given port.
	/// It is allowed to listen on multiple ports, so long as the port is not in use.
	/// Params:
	/// 	Port = The port to listen for connections on.
	void Listen(ushort Port) {
		synchronized(this) {			
			_State = ServerState.Listening;		
			if(FirstListen) {
				NativeReference.AddReference(cast(void*)this);
				task(&CheckTimeoutLoop).executeInNewThread();
				FirstListen = false;
			}	
			if((Version & IPVersion.IPv4) != 0)
				_ServerSockets ~= CreateSocket(IPVersion.IPv4, _Protocol, new InternetAddress(InternetAddress.ADDR_ANY, Port));
			if((Version & IPVersion.IPv6) != 0)
				_ServerSockets ~= CreateSocket(IPVersion.IPv6, _Protocol, new Internet6Address(Internet6Address.ADDR_ANY, Port));
			this.BegunListening.Execute(Port);
		}
	}

	/// Stops listening for connections, disables send and receives, attempts to disconnect all clients, and closes the server sockets.
	/// Not necessarily in any order.
	/// Params:
	/// 	Reason = The reason for the shutdown, usually used for logging purposes.
	/// 	WaitDuration = The amount of time to wait after initiating the calls to disconnect all clients, prior to disconnecting the server.
	void Disconnect(string Reason = "Server is shutting down.", Duration WaitDuration = dur!"msecs"(5000)) {	
		synchronized(this) {
			if(_State != ServerState.Listening)
				throw new InvalidStateException("Must be listening to disconnect the server.");
			FirstListen = true;
			NativeReference.RemoveReference(cast(void*)this);
			this.ShuttingDown.Execute(Reason);			
			foreach(ConnectionType Conn; _Connections) {
				Conn.Socket.Disconnect(Reason, null, null);
			}
			Thread.sleep(WaitDuration);
			foreach(AsyncSocket ServerSocket; _ServerSockets)
				ServerSocket.Disconnect(Reason, null, null);
		}
	}

protected:
	
	/// Creates a connection for the given socket.
	ConnectionType CreateConnection(AsyncSocket Socket) {
		return new ConnectionType(Socket);
	}

	AsyncSocket CreateSocket(IPVersion Version, NetProtocol Protocol, Address Addr) {
		ProtocolType SockProt;
		AddressFamily SockFam;
		SocketType SockType;

		if(Version == IPVersion.IPv4)
			SockFam = AddressFamily.INET;
		else if(Version == IPVersion.IPv6)
			SockFam = AddressFamily.INET6;
		else
			assert(0, "Unknown address version.");

		if(Protocol == NetProtocol.TCP) {
			SockType = SocketType.STREAM;
			SockProt = ProtocolType.TCP;
		} else if(Protocol == NetProtocol.UDP) {
			SockType = SocketType.DGRAM;
			SockProt = ProtocolType.UDP;
		} else
			assert(0, "Unknown protocol.");

		AsyncSocket Result = new AsyncSocket(SockFam, SockType, SockProt);
		Result.Bind(Addr);
		Result.Listen(128);
		Result.StartAccepting(cast(void*)Result, &OnConnectionRequested);
		
		return Result;
	}

	void OnConnectionRequested(void* State, AsyncSocket NewSocket) {		
		ConnectionType Conn = CreateConnection(NewSocket);		
		bool Rejected = false;
		size_t ExistingCount = 0;
		if(MaxConnectionsPerIP != size_t.max) {
			immutable(ubyte[]) Bytes = BytesForAddress(NewSocket.RemoteAddress);		
			synchronized(IpToConnectionsLock) {
				ExistingCount = IpToConnections.get(Bytes, 0);
			}
		}
		// We don't want to even call the connection checkers if they have too many connections open, to prevent a DoS with expensive checkers (such as ones that involve a database lookup for a blacklist).
		if(ExistingCount >= MaxConnectionsPerIP)
			Rejected = true;
		else {		
			auto Results = this._ConnectionRequested.Execute(Conn);
			foreach(bool Result; Results) {
				if(!Result) {
					Rejected = true;
					break;
				}			
			}
		}
		if(Rejected)
			OnConnectionRejected(Conn);
		else
			OnConnectionAccepted(Conn);
	}

	void OnConnectionAccepted(ConnectionType Conn) {
		// TODO: Remove this lock or lower the scope of it.
		synchronized(this) {			
			if(MaxConnectionsPerIP != size_t.max) {
				immutable(ubyte[]) IpBytes = BytesForAddress(Conn.Socket.RemoteAddress);
				synchronized(IpToConnectionsLock) {
					size_t* Existing = (IpBytes in IpToConnections);
					if(Existing is null) {
						IpToConnections[cast(immutable)IpBytes] = 1;
					} else if(*Existing > MaxConnectionsPerIP)
						Conn.Socket.Disconnect("Too many open connections.", null, null);
				}
			}
			_Connections.Add(Conn);			
			Conn.Socket.RegisterNotifyDisconnected(cast(void*)Conn, &OnConnectionDisconnected);
			Conn.RegisterReceiver(new CallbackOutput(cast(void*)Conn, &OnDataReceived));
			this._ClientConnected(Conn);
		}
	}

	void OnConnectionDisconnected(void* State, string Reason, int PlatformCode) {
		// TODO: Remove this lock or lower the scope of it.
		synchronized(this) {	
			ConnectionType Conn = cast(ConnectionType)State;		
			if(MaxConnectionsPerIP != size_t.max) {
				immutable(ubyte[]) IpBytes = BytesForAddress(Conn.Socket.RemoteAddress);
				synchronized(IpToConnectionsLock) {
					size_t* Existing = (IpBytes in IpToConnections);					
					if(Existing !is null) {
						*Existing = *Existing - 1;
						if(*Existing == 0)
							IpToConnections.remove(IpBytes);
					}						
				}
			}			
			Connections.Remove(Conn);
			this._ClientDisconnected(Conn, Reason);
		}
	}

	void OnConnectionRejected(ConnectionType Conn) {		
		Conn.Socket.Disconnect("The connection attempt was rejected.", null, null);
		this._ConnectionRejected(Conn); 		
	}

	void OnDataReceived(void* State, ubyte[] Data) {		
		ConnectionType Connection = cast(ConnectionType)State;
		this._DataReceived(Connection, Data);		
	}

	void CheckTimeoutLoop() {		
		while(_State == ServerState.Listening) {
			Duration TimeoutDur = TimeoutTime;
			if(TimeoutDur.total!"msecs" == 0) {
				Thread.sleep(dur!"seconds"(5));
				continue;
			}
						
			SysTime CurrTime = Clock.currTime();						
			foreach(ConnectionType Conn; _Connections) {					
				synchronized(Conn) {				
					if((CurrTime - Conn.LastReceive) >= TimeoutDur && Conn.Socket.State == SocketState.Connected) {				
						Conn.Socket.Disconnect("Connection did not send a message within TimeoutTime.", null, null);
					}
				}
			}			

			Thread.sleep(dur!"seconds"(5));
		}
	}
	
private:

	ServerState _State;
	NetProtocol _Protocol;
	IPVersion _Version;	

	LinkedList!ConnectionType _Connections;
	AsyncSocket[] _ServerSockets;

	size_t _MaxConnectionsPerIP = 16;
	size_t[immutable(ubyte[])] IpToConnections;
	Mutex IpToConnectionsLock;

			
	ConnectionRequestedEvent _ConnectionRequested;	
	ConnectionEvent _ClientConnected;
	ConnectionClosedEvent _ClientDisconnected;
	ConnectionEvent _ConnectionRejected;
	ServerShutdownEvent _ShuttingDown;
	ServerListenEvent _BegunListening;
	ConnectionDataEvent _DataReceived;

	Duration _TimeoutTime;
	 
	bool FirstListen = true;

	private immutable(ubyte[]) BytesForAddress(Address addr) { 
		auto name = addr.name();
		size_t nameLen = addr.nameLen();		
		immutable(ubyte[]) nameBytes = (cast(ubyte*)name)[0 .. nameLen].idup;
		return nameBytes;
	}
} 