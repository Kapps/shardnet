﻿module ShardNet.BackendConnection;
private import core.atomic;
public import ShardTools.Event;
import std.datetime;
public import ShardNet.IResponse;
public import ShardNet.IRequest;

/// Provides a connection to a single backend capable of handling requests.
/// A request is sent to the BackendConnection implementation, which is reponsible for actually handling the request when possible.
abstract class BackendConnection  {
 
public:

	alias Event!(void, IRequest) RequestSentEvent;
	alias Event!(void, IResponse) ResponseReceivedEvent;

	/// Initializes a new instance of the BackendConnection object.
	this(RequestType RequestsSupported) {
		this._RequestSent = new RequestSentEvent();
		this._ResponseReceived = new ResponseReceivedEvent();
		this._RequestsSupported = RequestsSupported;
	}

	/// Sends a request to this backend. When deriving, the base implementation must be called.
	/// Params:
	/// 	Request = The request being sent.
	abstract void SendRequest(IRequest Request) {
		_LastRequest = Clock.currTime();
		atomicOp!"+="(_NumRequests, 1);
		atomicOp!"+="(_PendingRequests, 1);		
		this.RequestSent.Execute(Request);
	}

	/// Sends a request to terminate the given request.
	/// A response should not be expected afterwards.
	/// Params:
	/// 	RequestToTerminate = The request to terminate.
	abstract void SendTerminateRequest(IRequest RequestToTerminate) {
		RequestToTerminate.Terminate();
		atomicOp!"-="(_PendingRequests, 1);
	}

	/// Called by the implementation to notify the connection a response was received.
	/// Params:
	/// 	Response = The response that was received from the backend.
	protected final void NotifyResponseReceived(IResponse Response) {
		OnResponseReceived(Response);
	}

	/// Gets the type of requests supported by this backend.
	@property RequestType RequestsSupported() const {
		return _RequestsSupported;
	}

	/// Ditto
	@property void RequestsSupported(RequestType Value) {
		_RequestsSupported = Value;
	}

	/// Called when a response is received from the backend.
	/// When deriving, the base implementation must be called.
	/// Params:
	/// 	Response = The response being received.
	protected abstract void OnResponseReceived(IResponse Response) {
		atomicOp!"-="(_PendingRequests, 1);		
		this.ResponseReceived.Execute(Response);
	}

	/// Gets a value indicating whether the backend is responding and capable of receiving requests.	
	@property abstract bool IsResponding();

	/// Gets the number of requests that are pending a response.
	@property final size_t PendingRequests() const {
		return _PendingRequests;
	}

	/// Gets the total number of requests sent through this BackendConnection, whether or not a response was received.
	@property final size_t TotalRequests() const {
		return _NumRequests;
	}

	/// Gets the time that the last request was sent, whether or not a response was received.
	@property final SysTime LastRequest() {
		return _LastRequest;
	}

	/// An event called when a request is received from the backend.
	final @property RequestSentEvent RequestSent() {
		return _RequestSent;
	}

	/// An event called when a response is received from the backend.
	/// Not every sent request will receive a response.
	final @property ResponseReceivedEvent ResponseReceived() {
		return _ResponseReceived;
	}
	
private:
	shared size_t _NumRequests;
	shared size_t _PendingRequests;
	SysTime _LastRequest;
	RequestType _RequestsSupported;
	RequestSentEvent _RequestSent;
	ResponseReceivedEvent _ResponseReceived;
}