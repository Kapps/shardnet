﻿module ShardNet.NetClient;
public import ShardNet.NetConnection;


/// Provides a client that can connect to any server using either IPv4 or IPv6 with a NetworkController.
/// The NetClient class itself does not introduce any message format, and thus can be used to connect to any server.
class NetClient(T : NetConnection)  {

public:
	/// Initializes a new instance of the NetClient object.
	this() {
		
	}

	/// Gets the connection to the server, or null if not connected.
	@property NetConnection!(T) Connection() {
		return _Connection;
	}

	/// Gets the state of the connection.
	@property ConnectionState State() {
		if(Connection !is null)
			return Connection.State;
		return ConnectionState.Disconnected;
	}
	
private:	
	NetConnection _Connection;	
}