﻿module ShardNet.RemoteBackend;
public import ShardNet.IRequestHandler;

public import ShardNet.Backend;


class RemoteBackend : Backend {

public:
	/// Initializes a new instance of the RemoteBackend object.
	this(IRequestHandler RequestHandler) {
		super(RequestHandler);
	}

protected:

	abstract void DataReceived(ubyte[] Data) {
		assert(Data.length > 13);
		ubyte[2] Header = Data[0..2];
	}
	
private:
}