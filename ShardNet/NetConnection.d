﻿module ShardNet.NetConnection;
private import core.atomic;
private import core.memory;
private import std.stdio;
private import ShardTools.ExceptionTools;
private import std.exception;
private import std.parallelism;
private import ShardTools.BufferPool;
private import std.algorithm;
private import std.array;
private import ShardTools.ArrayOps;
private import ShardIO.MemoryOutput;
private import ShardIO.StreamInput;
private import ShardTools.Queue;
private import ShardIO.SocketInput;
private import ShardIO.SocketOutput;
private import ShardIO.MemoryInput;
public import ShardIO.IOAction;
public import ShardTools.Buffer;
public import ShardIO.AsyncSocket;
private import std.datetime;
private import ShardTools.Event;
private import std.socket;
import ShardTools.Untyped;

// For bug 314.
alias core.time.Duration Duration;

/// Determines the protocol to use for networking.
enum NetProtocol {
	/// A reliable, ordered, stream-based, protocol with more overhead than UDP.
	TCP = 1,	
	/// An unreliable, unordered, packet-based, protocol with less overhead than TCP.
	UDP = 2
}

enum IPVersion {
	IPv4 = 1,
	IPv6 = 2,
	Both = IPv4 | IPv6
}

enum ConnectionState {
	Disconnected = 0,
	Connecting = 1,
	Connected = 2
}

/// Represents a connection from the local machine to a remote endpoint.
/// Outgoing data is buffered and written to through a stream, whereas incoming data is passed to an event.
class NetConnection  {

public:
		
	/+ /// Gets a stream used for sending messages.
	/// All writes here are buffered until SendMessages is called on a NetController for this Connection.
	@property Buffer SendBuffer() {
		return _Output;
	}
	
	/// Sends the messages currently buffered to the endpoint.
	void SendMessages() {
		//Output.Flush();
	}+/

	/// Creates a NetConnection for the given socket.
	this(AsyncSocket Socket) {
		this._Socket = Socket;
		this._State = ConnectionState.Connected;		
		this._LastReceive = Clock.currTime();
		this.SendQueue = new Queue!(IOAction);
		//static __gshared size_t Count;
		//writeln("Created connection number ", atomicOp!("+=", size_t, int)(Count, 1), ".");
		_Socket.RegisterNotifyDisconnected(cast(void*)this, &OnDisconnect);
		RequestData();
	}

	/// Gets the time that the last message was received.
	@property SysTime LastReceive() {
		return _LastReceive;
	}

	/// Gets the state of the connection.
	@property ConnectionState State() const {
		return _State; 
	}

	/// Gets the socket used for this connection.
	@property AsyncSocket Socket() {
		return _Socket;
	}
	
	/// Returns whether State is Disconnected.
	@property bool IsDisconnected() const {
		return State == ConnectionState.Disconnected;
	}
	 
	/// Sends an InputSource to the given endpoint, returning the IOAction used for sending the data.
	/// If multiple sends are sent, they will be added to a queue and sent after the previous send completes.
	/// Params:
	/// 	Input = The input to send.		
	IOAction Send(InputSource Input) {
		// TODO: Allow parallel sends when the caller allows them.
		/+if(this.__monitor is null) {
			static __gshared size_t Count;
			writeln("Created monitor number ", atomicOp!("+=", size_t, int)(Count, 1), ".");
		}+/
		synchronized(this) {			
			CheckDisconnected();
			OutputSource Output = new SocketOutput(Socket);		
			IOAction Action = new IOAction(Input, Output);
			QueueSend(Action);
			return Action;
		}
	}

	/// Ditto
	IOAction Send(ubyte[] Data) {
		return Send(new MemoryInput(Data, false));
	}

	/// Receives data from this connection, sending the results into the given OutputSource and returning the IOAction being used for this operation.
	/// Multiple receivers can be registered for any connection, and the data will be sent to each receiver.
	/// All data received will be sent to all non-complete receivers, thus you may unregister a receiver by completing the IOAction associated with it.
	/// For the equivalent of an OnReceived event, consider using a CallbackOutput.
	/// Params:
	/// 	Output = The source to send the output to.	
	IOAction RegisterReceiver(OutputSource Output) {
		synchronized(this) {			
			assert(Output !is null);
			CheckDisconnected();
			StreamInput Input = new StreamInput(FlushMode.PerWrite);
			IOAction Action = new IOAction(Input, Output);
			Action.NotifyOnComplete(Untyped.init, &OnReceiverComplete);			
			ReceiverInputs ~= Input;
			Action.Start();			
			return Action;
		}
	}

	/// Gets or sets the size of the receive buffer.
	@property size_t ReceiveBufferSize() const {
		return _ReceiveBufferSize;
	}

	/// Ditto
	@property void ReceiveBufferSize(size_t Value) {
		_ReceiveBufferSize = Value;
	}

protected:

	void OnActionComplete(Untyped State, AsyncAction Action, CompletionType Status) {		
		synchronized(this) {
			assert(CurrentAction == Action);
			CurrentAction = null;
			if(this.SendQueue.Count > 0) {
				IOAction Next = SendQueue.Dequeue();
				RunAction(Next);
			}
		}
	}

	void QueueSend(IOAction Action) {
		synchronized(this) {				
			if(SendQueue.Count == 0 && CurrentAction is null)
				RunAction(Action);
			else
				SendQueue.Enqueue(Action);
		}
	}

	void OnReceiverComplete(Untyped State, AsyncAction Action, CompletionType Status) {
		synchronized(this) {
			size_t Index = IndexOf(ReceiverInputs, cast(StreamInput)(cast(IOAction)Action).Input);
			assert(Index != -1);
			ReceiverInputs = array(remove(ReceiverInputs, Index));
		}
	}

	void OnDataReceived(void* State, ubyte[] Data) {
		//GC.disable();
		//scope(exit) GC.enable();
		synchronized(this) {			
			//writeln("Received ", Data.length, " bytes to NetConnection.");
			if(IsDisconnected) {
				// debug writeln("Received ", Data.length, " bytes while disconnected.");
				return; // We don't want more data after being disconnected.
			}
			//RequestData();
			this._LastReceive = Clock.currTime();
			Buffer buff = cast(Buffer)State;
			// TODO: Probably just reuse the buffer, not release / acquire every time.
			scope(exit)
				BufferPool.Global.Release(buff);
			foreach(StreamInput Input; ReceiverInputs) {				
				Input.Write(Data);
			}			
			RequestData();
		}
	}

	void OnDisconnect(void* State, string Reason, int PlatformCode) {
		synchronized(this) {
			if(_State == ConnectionState.Disconnected)
				return;
			_State = ConnectionState.Disconnected;
			IOAction Action;
			while(SendQueue.Count > 0) {
				Action = SendQueue.Dequeue();				
				Action.Abort();
			}			
			if(CurrentAction !is null)
				CurrentAction.Abort();
		}
	}

	void RunAction(IOAction Action) {			
		debug CheckDisconnected();
		CurrentAction = Action;		
		Action.NotifyOnComplete(Untyped.init, &OnActionComplete);
		Action.Start();
	}
	
	void RequestData() {
		debug CheckDisconnected();
		Buffer buff = BufferPool.Global.Acquire(ReceiveBufferSize);
		//taskPool.put(task(&Socket.Receive, buff.FullData, cast(void*)buff, &OnDataReceived));
		Socket.Receive(buff.FullData, cast(void*)buff, &OnDataReceived);
	}
	
private:

	void CheckDisconnected() {
		if(_State == ConnectionState.Disconnected)
			throw new SocketException("The connection to perform an action on was disconnected.");
	}
	
	SysTime _LastReceive;		
	ConnectionState _State;
	AsyncSocket _Socket;	
	size_t _ReceiveBufferSize = 8192;

	Queue!IOAction SendQueue;
	IOAction CurrentAction;

	StreamInput[] ReceiverInputs;			
}