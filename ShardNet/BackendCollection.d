﻿module ShardNet.BackendCollection;
private import ShardTools.ArrayOps;
private import std.exception;
public import ShardNet.BackendConnection;
import std.algorithm;
import std.array;


/// A collection of backends.
class BackendCollection  {

public:

	/// An event for when a backend is added or removed from a collection.
	alias Event!(void, BackendCollection, BackendConnection) BackendEvent;

	/// Initializes a new instance of the BackendCollection object.
	this() {		
		this._BackendAdded = new BackendEvent();
		this._BackendRemoved = new BackendEvent();
	}	

	/// Indexer for the BackendCollection.
	BackendConnection opIndex(size_t Index) {
		return _Backends[Index];
	}

	/// Foreach iterator for the BackendCollection.
	int opApply(int delegate(ref BackendConnection) Callback) {
		int Result = 0;
		foreach(BackendConnection con; _Backends)
			if((Result = Callback(con)) != 0)
				break;
		return Result;
	}

	/// Adds a backend to the collection.
	/// Params:
	/// 	Connection = The connection to the backend.
	void Add(BackendConnection Connection) {
		enforce(Connection, "Connection may not be null.");
		enforce(!Contains(_Backends, Connection), "The backend already exists.");
		_Backends ~= Connection;
		_BackendAdded.Execute(this, Connection);
	}

	/// Removes a backend from the collection, returning whether it was found to be removed.
	/// Params:
	/// 	Connection = The connection to remove.
	bool Remove(BackendConnection Connection) {
		size_t Index = _Backends.IndexOf(Connection);
		if(Index == -1)
			return false;
		_Backends = array(_Backends.remove(Index));
		_BackendRemoved.Execute(this, Connection);
		return true;
	}

	/// An event called when a backend is added.
	@property BackendEvent Added() {
		return _BackendAdded;
	}

	/// An event called when a backend is removed.
	@property BackendEvent Removed() {
		return _BackendRemoved;
	}

	// TODO: Allow IRequest.
	/// Returns a backend that can be used to handle the given request.
	/// Params:
	/// 	Request = The request to handle.
	BackendConnection BackendForRequest(IRequest Request) {
		BackendConnection NextConnection = null;
		size_t MinRequests = size_t.max;
		foreach(BackendConnection Connection; _Backends) {
			if(!Connection.IsResponding)
				continue;
			if((Connection.RequestsSupported & Request.Type) == 0)
				continue;
			if(Connection.PendingRequests < MinRequests) {
				NextConnection = Connection;
				MinRequests = Connection.PendingRequests;
			}
		}
		return NextConnection;
	}
	
private:	
	BackendConnection[] _Backends;
	BackendEvent _BackendAdded;
	BackendEvent _BackendRemoved;
}