﻿module OpenSSL;
import std.conv : to;
import std.stdio;
import core.stdc.stdio : fopen;
 
/+version(Windows) {
	// Because we need to call OPENSSL_AppLink, but it's not part of the library.
	// So instead of linking a separate library (which may not even be possible), 
	// just port it to D instead. Some ugliness needed to make this work.
	
	// ...and in the end, it still doesn't. :( Here for future use.
	extern(C) {
		int open(const char*, int, int);
		int close(int);
		int read(int, void*, uint);
		int write(int, const void*, uint);
		size_t lseek(int, size_t, int);			
	}
	
	enum : int { // Values used by DMC.
		_O_BINARY = 0x8000,
		_O_TEXT = 0x4000
	}
	enum : int {
		APPLINK_STDIN = 1,
		APPLINK_STDOUT = 2,
		APPLINK_STDERR = 3,
		APPLINK_FPRINTF = 4,
		APPLINK_FGETS = 5,
		APPLINK_FREAD = 6,
		APPLINK_FWRITE = 7,
		APPLINK_FSETMOD = 8,
		APPLINK_FEOF = 9,
		APPLINK_FCLOSE = 10,
		APPLINK_FOPEN = 11,
		APPLINK_FSEEK = 12,
		APPLINK_FTELL = 13,
		APPLINK_FFLUSH = 14,
		APPLINK_FERROR = 15,
		APPLINK_CLEARERR = 16,
		APPLINK_FILENO = 17,
		APPLINK_OPEN = 18,
		APPLINK_READ = 19,
		APPLINK_WRITE = 20,
		APPLINK_LSEEK = 21,
		APPLINK_CLOSE = 22,
		APPLINK_MAX = 22
	}
	
	void* app_stdin() { return cast(void*)&stdin; }
	void* app_stdout() { return cast(void*)&stdout; }
	void* app_stderr() { return cast(void*)&stderr; }
	int app_feof(FILE* fp) { return feof(fp); }
	int app_ferror(FILE* fp) { return ferror(fp); }
	void app_clearerr(FILE* fp) { clearerr(fp); }
	int app_fileno(FILE* fp) { return _fileno(fp); }
	int app_fsetmod(FILE* fp, char mod) {
		return _setmode(fp._file, mod  == 'b' ? _O_BINARY : _O_TEXT);
	}
	
	extern(C) export __gshared void*[APPLINK_MAX+1] OPENSSL_ApplinkTable;
	extern(C) export __gshared bool Executed = false;
	extern(C) export void** OPENSSL_AppLink() {										
		if(!Executed) {
			OPENSSL_ApplinkTable[0] = cast(void*)APPLINK_MAX;
			OPENSSL_ApplinkTable[APPLINK_STDIN] = &app_stdin;
			OPENSSL_ApplinkTable[APPLINK_STDOUT] = &app_stdout;
			OPENSSL_ApplinkTable[APPLINK_STDERR] = &app_stderr;
			OPENSSL_ApplinkTable[APPLINK_FPRINTF] = &fprintf;
			OPENSSL_ApplinkTable[APPLINK_FGETS] = &fgets;
			OPENSSL_ApplinkTable[APPLINK_FREAD] = &fread;
			OPENSSL_ApplinkTable[APPLINK_FWRITE] = &fwrite;
			OPENSSL_ApplinkTable[APPLINK_FSETMOD] = &app_fsetmod;
			OPENSSL_ApplinkTable[APPLINK_FEOF] = &app_feof;
			OPENSSL_ApplinkTable[APPLINK_FCLOSE] = &fclose;
			
			OPENSSL_ApplinkTable[APPLINK_FOPEN] = &fopen;
			OPENSSL_ApplinkTable[APPLINK_FSEEK] = &fseek;
			OPENSSL_ApplinkTable[APPLINK_FTELL] = &ftell;
			OPENSSL_ApplinkTable[APPLINK_FFLUSH] = &fflush;
			OPENSSL_ApplinkTable[APPLINK_FERROR] = &app_ferror;
			OPENSSL_ApplinkTable[APPLINK_CLEARERR] = &app_clearerr;
			OPENSSL_ApplinkTable[APPLINK_FILENO] = &app_fileno;
			OPENSSL_ApplinkTable[APPLINK_OPEN] = &open;
			OPENSSL_ApplinkTable[APPLINK_READ] = &read;
			OPENSSL_ApplinkTable[APPLINK_WRITE] = &write;
			OPENSSL_ApplinkTable[APPLINK_LSEEK] = &lseek;
			OPENSSL_ApplinkTable[APPLINK_CLOSE] = &close;
			Executed = true;
		}
		writefln("APPLINK: " ~ to!string(OPENSSL_ApplinkTable) ~ ".");
		return OPENSSL_ApplinkTable.ptr;
	}
}+/

extern(C) {

	alias int function(const SSL*, ubyte*, uint*) GEN_SESSION_CB;

	enum : uint {
		SSL_ERROR_NONE = 0,
		SSL_ERROR_SSL = 1,
		SSL_ERROR_WANT_READ = 2,
		SSL_ERROR_WANT_WRITE = 3,
		SSL_ERROR_WANT_X509_LOOKUP = 4,
		SSL_ERROR_SYSCALL = 5,
		SSL_ERROR_ZERO_RETURN = 6,
		SSL_ERROR_WANT_CONNECT = 7,
		SSL_ERROR_WANT_ACCEPT = 8
	}
	
	struct CRYPTO_EX_DATA {
		void* stack;
		int dummy;
	}

	struct SSL {
		int Version; // version reserved.
		int type;
		const SSL_METHOD* method;
		void* rbio, wbio, bbio;
		int rwstate;
		int in_handshake;
		int function(SSL*) handshake_func;
		int server;
		int new_session;
		int quiet_shutdown;
		int shutdown;
		int state;
		int rstate;
		void* init_buf; // BUF_MEM
		void* init_msg; // void
		int init_num;
		int init_off;
		ubyte* packet;
		int packet_length;
		void* s2; // ssl2_state_st
		void* s3; // ssl3_state_st
		void* d1; // dtsl1_state_st
		int read_ahead;
		void function(int, int, int, const void*, size_t, SSL*, void*) msg_callback;
		void* msg_callback_arg; // void
		int hit;
		void* param; // X509_VERIFY_PARAM
		void* cipher_list; // STACK_OF(SSL_CIPHER)
		void* cipher_list_by_id; // STACK_OF(SSL_CIPHER)
		int mac_flags;
		void* enc_read_ctx; // EVP_CIPHER_CTX
		void* read_hash; // EVP_MD_CTX
		void* expand; // varies
		void* enc_write_ctx; // EVP_CIPHER_CTX
		void* write_hash; // EVP_MD_CTX
		void* compress; // varies
		void* cert; // cert_st
		uint sid_ctx_length;
		ubyte[32] sid_ctx;
		void* ssl_session; // SSL_SESSION
		GEN_SESSION_CB generate_session_id;
		int verify_mode;
		int function(int, void*) verify_callback; // X509_STORE_CTX
		void function(const SSL*, int, int) info_callback;
		int error;
		int error_code;
		void* kssl_ctxt;
		uint function(SSL*, const char*, char*, uint, char*, uint) psk_client_callback;
		uint function(SSL*, const char*, uint) psk_server_callback;
		SSL_CTX* ctx;
		int Debug; // debug reserved
		size_t verify_result;
		CRYPTO_EX_DATA ex_data;
		void* client_CA;
		int references;
		size_t options;
		size_t mode;
		size_t max_cet_list;
		int first_packet;
		int client_version;
		uint max_send_fragment;
		void function(int, int, char*, int, void*) tlsext_debug_cb;
		void* tlsext_debug_arg;
		char* tlsext_hostname;
		int servername_done;
		int tlsext_status_type;
		int tlsext_status_expected;
		void* tlsext_ocsp_ids;
		void* tlsext_oscp_exts;
		char* tlsext_ocsp_resp;
		int tlsext_oscp_resple;
		int tlsext_ticket_expected;
		size_t tlsext_ecpointformatlist_length;
		char* tlsext_ecpointformatlist;
		size_t tlsext_ellipticcurvelist_length;
		char* tlsext_ellipticcurvelist;
		void* tlsext_opaque_prf_input;
		size_t tlsext_opaque_prf_input_len;
		void* tlsext_session_ticket;
		void* tls_session_ticket_ext_cb;
		void* tls_session_ticket_ext_cb_arg;
		void* tls_session_secret_cb;
		void* tls_session_secret_cb_arg;
		SSL_CTX* initial_ctx;
	}
	
	/+ struct SSL_STACK { // ssl_stack_st_whatever. We don't implement it, just fake it.
		void[(4 * int.sizeof) + size_t.sizeof] Buffer;
	}+/
	
	struct SSL_CTX {
		const SSL_METHOD* method;
		void* cipher_list;
		void* cipher_list_by_id;
		void* cert_store; // x509_store_st
		void* sessions; // LHASH_OF(SSL_SESSION)
		size_t session_cache_size;
		void* session_cache_head; // ssl_session_st
		void* session_cache_tail; // ssl_session_st
		int session_cache_mode;
		size_t session_timeout;
		int function(SSL*, void*) new_session_cb;
		void function(SSL_CTX*, void*) remove_session_cb;
		void* function(SSL*, ubyte*, int, int*) get_session_cb;
		struct {	
			int sess_connect, sess_connect_renegotiate, sess_connect_good, sess_accept, sess_accept_renegotiate, sess_accept_good, sess_miss, sess_timeout, sess_cache_full, sess_hit, sess_cb_hit;
		}
		int references;
		int function(void*, void*) app_verify_callback;
		void* app_verify_arg;
		void* default_passwd_callback;
		void* default_passwd_callback_usrdata;
		int function(SSL*, void**, void**) client_cert_cb;
		int function(SSL*, char*, uint*) app_gen_cookie_cb;
		int function(SSL*, char*, uint) app_verify_cookie_cb;
		CRYPTO_EX_DATA ex_data;
		void* rsa_md5;
		void* md5;
		void* sha1;
		void* extra_certs;
		void* comp_methods;
		void function(SSL*, int, int) info_callback;
		void* client_CA;
		size_t options, mode;
		size_t max_cert_list;
		void* cert; 
		int		read_ahead;
		void function(int, int, int, const void*, size_t, SSL*, void*) msg_callback;
		void* msg_callback_arg;
		int verify_mode;
		uint sid_ctx_length;
		char[32] sid_ctx;
		int function(int, void*) default_verify_callback;
		GEN_SESSION_CB generate_session_id;
		void* param;
		int quiet_shutdown;
		uint max_send_fragment;
		void* client_cert_engine;
		int function(SSL*, int*, void*) tlsext_servername_callback;
		void* tlsext_servername_arg;
		char[16] tlsext_tick_key_name;
		char[16] tlsext_tick_hmac_key;
		char[16] tlsext_tick_aes_key;
		int function(SSL*, void*, size_t, void*) tlsext_opaque_prf_input_callback;
		void* tlsext_opaque_prf_input_callback_arg;
		char* psk_identity_hint;
		uint function(SSL*, const char*, char*, uint, char*, uint) psk_client_callback;
		uint function(SSL*, const char*, char*, uint) psk_server_callback;
		uint freeist_max_len;
		void* wbuf_free_list;
		void* rbuf_free_list;
	}
	
	struct SSL_CIPHER {
		int valid;
		const(char*) name;
		size_t id;
		size_t algorithm_mkey, algorithm_auth, algorithm_enc, algorithm_mac, algorithm_ssl;
		size_t algo_strength;
		size_t algorithm2;
		int strength_bits;
		int alg_bits;
	}
	
	struct SSL_METHOD {
		int Version; // version reserved keyword
		int function(SSL*) ssl_new;
		void function(SSL*) ssl_clear;
		void function(SSL*) ssl_free;
		int function(SSL*) ssl_accept;
		int function(SSL*) ssl_connect;
		int function(SSL*, void*, int) ssl_read;
		int function(SSL*, void*, int) ssl_peek;
		int function(SSL*, const void*, int) ssl_write;
		int function(SSL*) ssl_shutdown;
		int function(SSL*) ssl_renegotiate;
		int function(SSL*) ssl_renegotiate_check;
		size_t function(SSL*, int, int, int, size_t, int, int*) ssl_get_message;
		int function(SSL*, int, ubyte*, int, int) ssl_read_bytes;
		int function(SSL*, int, const void*, int) ssl_write_bytes;
		int function(SSL*) ssl_dispatch_alert;
		size_t function(SSL*, int, size_t, void*) ssl_ctrl;
		size_t function(SSL_CTX*, int, size_t, void*) ssl_ctx_ctrl;
		const(SSL_CIPHER*) function(const char*) get_cipher_by_char;
		int function(const SSL_CIPHER*, char*) put_cipher_by_char;
		int function(SSL*) ssl_pending;
		int function() num_ciphers;
		const(SSL_CIPHER*) function(uint) get_cipher;
		const(SSL_METHOD*) function(int) get_ssl_method;
		size_t function() get_timeout;
		void* ssl3_enc; // ssl3_enc_method
		int function() ssl_version;
		size_t function(SSL*, int, void function()) ssl_callback_ctrl;
		size_t function(SSL_CTX*, int, void function()) ssl_ctx_callback_ctrl;
	}

	enum : int {
		SSL_FILETYPE_PEM = 1,
		SSL_FILETYPE_ASN1 = 2 
	}	

	void SSL_load_error_strings();
	int SSL_library_init();
	const(SSL_METHOD*) SSLv23_server_method();
	SSL_CTX* SSL_CTX_new(const SSL_METHOD*);	
	// All calls that involve a FILE* are commented out until APPLINK decides that it feels like being called.
	// Use the BIO* functions instead.
	//int	SSL_CTX_use_certificate_file(SSL_CTX*, const char*, int);
	//int	SSL_CTX_use_PrivateKey_file(SSL_CTX*, const char*, int);
	int SSL_CTX_check_private_key(const SSL_CTX*);
	SSL* SSL_new(SSL_CTX*);
	int	SSL_set_fd(SSL*, int);
	int SSL_accept(SSL*);
	int	SSL_get_error(const SSL*, int);
	void SSL_free(SSL*);	

	int SSL_connect(SSL*);
	int SSL_read(SSL*, void*, int);
	int SSL_peek(SSL*, void*, int);
	int SSL_write(SSL*, const void*, int);
	const(char*) SSL_CIPHER_get_name(const SSL_CIPHER*);
	const(SSL_CIPHER*) SSL_get_current_cipher(const SSL*);	
	
	size_t ERR_get_error();
	char* ERR_error_string(size_t, char*);
	//void ERR_print_errors_fp(FILE*);	
	
	void* BIO_s_file();
	void* BIO_new_file(const char*, const char*);
	void* BIO_new_fp(FILE*, int); // : BIO
	int BIO_read_filename(void*, char*); // BIO
	int BIO_write_filename(void*, char*); // BIO
	int BIO_append_filename(void*, char*); // BIO
	int BIO_rw_filename(void*, char*); // BIO
	int BIO_free(void*); // BIO
	
	void* PEM_read_bio_X509(void*, void**, void*, void*); // BIO, X509, pem_password_cb, void : X509	
	void* PEM_read_X509(FILE*, void**, void*, void*);
	void* PEM_read_bio_PrivateKey(void*, void**, void*, void*); // BIO, X509, pem_password_cb, void : EVP_PKEY
	void* PEM_read_PrivateKey(FILE*, void**, void*, void*);
	int SSL_CTX_use_PrivateKey(SSL_CTX*, void*); // EVP_KEY
	int SSL_CTX_use_certificate(SSL_CTX*, void*); // X509
	void SSL_set_accept_state(SSL*);

	/+version(Windows) {
		int CRYPTO_set_mem_functions(void* function(size_t), void* function(void*, size_t), void function(void*));	
	}+/	
	
	/+const(char*) SSL_get_cipher(SSL* ssl) {
		return SSL_CIPHER_get_name(SSL_get_current_cipher(ssl));
	}+/
}